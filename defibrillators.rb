def to_rad(input)
    degree = input.chomp.gsub(',', '.')
    degree.to_f * Math::PI / 180
end

min_distance = Float::MAX
answer = ""

lon = to_rad(gets)
lat = to_rad(gets)
n = gets.to_i

n.times do
  defib = gets.chomp

  defib_array = defib.split(';')
  defib_lon = to_rad(defib_array[-2])
  defib_lat = to_rad(defib_array[-1])

  x = (defib_lon - lon) * (Math.cos((defib_lat + lat) / 2))
  y = defib_lat - lat

  distance = Math.sqrt(x**2 + y**2) * 6371
  if distance < min_distance
    min_distance = distance
    answer = defib_array[1]
  end
end

puts "#{answer}"
