a, b, c, d = gets.split(" ").collect { |x| x.to_i }
n = gets.to_i

ins = []
n.times do
  ins << gets.chomp.split(' ')
end

def reg(value)
    return (value == "a" || value == "b" || value == "c" || value == "d")
end

i = 0
while i < ins.length()
    case ins[i][0]
    when "MOV"
        binding.local_variable_set(
            reg(ins[i][1]) ? ins[i][1] : ins[i][1].to_i,
            reg(ins[i][2]) ? binding.local_variable_get(ins[i][2]) : ins[i][2].to_i
        )
    when "ADD"
        binding.local_variable_set(
            reg(ins[i][1]) ? ins[i][1] : ins[i][1].to_i,
            (reg(ins[i][2]) ? binding.local_variable_get(ins[i][2]) : ins[i][2].to_i) +
            (reg(ins[i][3]) ? binding.local_variable_get(ins[i][3]) : ins[i][3].to_i),
        )
    when "SUB"
        binding.local_variable_set(
            reg(ins[i][1]) ? ins[i][1] : ins[i][1].to_i,
            (reg(ins[i][2]) ? binding.local_variable_get(ins[i][2]) : ins[i][2].to_i) -
            (reg(ins[i][3]) ? binding.local_variable_get(ins[i][3]) : ins[i][3].to_i),
        )
    when "JNE"
        x = reg(ins[i][2]) ? binding.local_variable_get(ins[i][2]) : ins[i][2].to_i
        y = reg(ins[i][3]) ? binding.local_variable_get(ins[i][3]) : ins[i][3].to_i
        z = reg(ins[i][1]) ? binding.local_variable_get(ins[i][1]) : ins[i][1].to_i
        if x != y
            i = z
            next
        end
    end
    i += 1
end

puts "#{a} #{b} #{c} #{d}"
