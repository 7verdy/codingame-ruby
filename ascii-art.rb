def pos(letter)
  actual = (letter.upcase).ord - 65
  if actual > 25 || actual < 0
    actual = 26
  end
  return actual
end

l = gets.to_i
h = gets.to_i
t = gets.chomp
arr = []
h.times do
  arr << gets.chomp
end
t.upcase!

answer = ""
h.times do |i|
  t.split('').each do |c|
    actual = pos(c)
    answer << arr[i][(actual * l) .. ((actual * l) + l - 1)]
  end
  answer << "\n"
end

puts "#{answer}"
