r_1 = gets.to_i

for i in (r_1 - 1..0).step(-1)
    done = (r_1 == i + i.digits.sum)
    if done
        puts "YES"
        return
    end
end

puts "NO"
