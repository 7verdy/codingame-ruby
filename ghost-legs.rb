lines = []
w, h = gets.split(" ").collect { |x| x.to_i }
h.times do
  lines << gets.chomp
end

ending = lines[-1].split(" ")
starting = lines[0].split(" ")
current = ending.clone

lines[1..-2].each do |line|
    moved = Array.new(current.length(), false)
    (0..w-1).step(3) do |x|
        if line[x + 1] == '-'
            for j in 0..current.length()-1
                if current[j] == ending[x / 3] && !moved[j]
                    current[j] = ending[ending.find_index(current[j]) + 1]
                    moved[j] = true
                end
            end
        elsif line[x - 1] == '-'
            for j in 0..current.length()-1
                if current[j] == ending[x / 3] && !moved[j]
                    current[j] = ending[ending.find_index(current[j]) - 1]
                    moved[j] = true
                end
            end
        end
    end
end

for i in 0..(current.length() - 1)
    puts "#{starting[i]}#{current[i]}"
end
