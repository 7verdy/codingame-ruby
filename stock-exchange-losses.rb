n = gets.to_i
v = gets.split(" ").collect { |el| el.to_i }

loss = 0

for i in (0..n-1)
    local_max = v[i]
    local_min = Float::MAX
    for j in (i+1..n-1)
        if v[j] >= local_max
            break
        end
        if v[j] < v[i] && v[j] < local_min
            local_min = v[j]
        end
    end
    loss = [loss, -(local_max - local_min)].min
end

puts "#{loss}"
